
-- Dumping database structure for ratings_service
DROP DATABASE IF EXISTS `ratings_service`;
CREATE DATABASE IF NOT EXISTS `ratings_service` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ratings_service`;

-- Dumping structure for table ratings_service.rating
DROP TABLE IF EXISTS `Rating`;
CREATE TABLE IF NOT EXISTS `Rating` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rating` float unsigned NOT NULL DEFAULT '0',
  `comment` varchar(500) NOT NULL DEFAULT '0',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

