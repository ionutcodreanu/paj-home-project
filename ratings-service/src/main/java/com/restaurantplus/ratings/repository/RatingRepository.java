package com.restaurantplus.ratings.repository;

import com.restaurantplus.ratings.model.Rating;

import java.util.List;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
public interface RatingRepository {

  /**
   * Get all ratings from repository
   */
  List<Rating> findAll();

  /**
   * Get only one rating entity based od rating internal identifier
   */
  Rating findById(Long ratingId);

  /**
   * Persist a rating
   */
  Rating create(Rating rating);

  /**
   * Update a rating
   */
  Rating update(Rating rating);

  /**
   * Delete a rating
   */
  void delete(Long ratingId);
}
