package com.restaurantplus.ratings.repository;

import com.restaurantplus.ratings.model.Rating;
import org.apache.commons.beanutils.BeanUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
public class RatingRepositoryJpa implements RatingRepository {

  private static EntityManager em;

  public RatingRepositoryJpa() {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Ratings");
    em = emf.createEntityManager();
  }

  @Override
  public List<Rating> findAll() {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Rating> cq = cb.createQuery(Rating.class);
    Root<Rating> rootEntry = cq.from(Rating.class);
    CriteriaQuery<Rating> all = cq.select(rootEntry);
    TypedQuery<Rating> allQuery = em.createQuery(all);
    return allQuery.getResultList();
  }

  @Override
  public Rating findById(Long ratingId) {
    return em.find(Rating.class, ratingId);
  }

  @Override
  public Rating create(Rating rating) {
    em.getTransaction().begin();
    em.persist(rating);
    em.getTransaction().commit();
    return em.find(Rating.class, rating.getId());
  }

  @Override
  public Rating update(Rating rating) {
    Rating attached = em.find(Rating.class, rating.getId());
    em.getTransaction().begin();
    if (attached == null) {
      attached = new Rating();
      em.persist(attached);
    }
    try {
      BeanUtils.copyProperties(attached, rating);
    } catch (IllegalAccessException exc) {
      System.out.println("Log here");
    } catch (InvocationTargetException exc) {
      System.out.println("Log here2");
    }
    em.getTransaction().commit();

    return attached;
  }

  @Override
  public void delete(Long ratingId) {
    Rating attached = em.find(Rating.class, ratingId);
    em.getTransaction().begin();
    em.remove(attached);
    em.getTransaction().commit();
  }
}
