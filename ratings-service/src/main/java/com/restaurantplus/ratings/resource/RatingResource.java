package com.restaurantplus.ratings.resource;

import com.restaurantplus.ratings.model.Rating;
import com.restaurantplus.ratings.repository.RatingRepository;
import com.restaurantplus.ratings.repository.RatingRepositoryJpa;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
@Path("ratings")
public class RatingResource {

  private RatingRepository repository = new RatingRepositoryJpa();

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Rating> findAllRatings() {
    return this.repository.findAll();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{ratingId}")
  public Rating findRating(@PathParam("ratingId") Long ratingId) {
    return this.repository.findById(ratingId);
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Rating createRating(Rating rating) {
    return repository.create(rating);
  }

  @PUT
  @Path("{ratingId}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Rating updateRating(Rating rating, @PathParam("ratingId") Long ratingId) {
    rating.setId(ratingId);
    return repository.update(rating);
  }

  @DELETE
  @Path("{ratingId}")
  @Produces(MediaType.APPLICATION_JSON)
  public void deleteRating(@PathParam("ratingId") Long ratingId) {
    repository.delete(ratingId);
  }
}
