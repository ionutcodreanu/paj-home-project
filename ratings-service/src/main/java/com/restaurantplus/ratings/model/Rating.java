package com.restaurantplus.ratings.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
@Entity
@XmlRootElement
public class Rating {

  @Id
  @GeneratedValue
  @Basic(optional = false)
  private Long id;

  private Double rating;
  private String comment;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getRating() {
    return rating;
  }

  public void setRating(Double rating) {
    this.rating = rating;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }
}
