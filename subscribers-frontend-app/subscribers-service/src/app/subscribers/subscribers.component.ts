import {Component, OnInit} from '@angular/core';
import {Subscribers} from "../subscribers";
import {SubscribersService} from "../subscribers.service";
import {SubscriberOption} from "../subscriber-option";

@Component({
  selector: 'app-subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.css']
})
export class SubscribersComponent implements OnInit {


  subscribers: Subscribers[];
  subscriberModel = new Subscribers();
  errorMessage: string;

  constructor(private subscribersService: SubscribersService) {
  }

  getSubscribers(): void {
    this.subscribersService
      .get()
      .then(
        subscribers => {
          this.subscribers = subscribers;
        },
        error => {
          this.errorMessage = <any>error;
        }
      );
  }

  delete(subscriber: Subscribers): void {
    this.subscribersService
      .delete(subscriber.id)
      .then(() => {
        this.subscribers = this.subscribers.filter(h => h !== subscriber);
      });
  }

  toggleEdit(subscriber: Subscribers): void {
    this.subscriberModel = subscriber;
  }

  ngOnInit(): void {
    this.getSubscribers();
  }


  private edit(): void {
    this.subscribersService.update(this.subscriberModel).then(() => {
      this.subscriberModel = new Subscribers();
    });
  }

}
