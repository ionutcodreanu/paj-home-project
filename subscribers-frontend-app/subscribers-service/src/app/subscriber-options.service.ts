import {Injectable} from '@angular/core';
import {SubscriberOption} from "./subscriber-option";
import {Http, Headers} from "@angular/http";
import {environment} from '../environments/environment';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SubscriberOptionsService {
  private serviceUrl = environment.API_BASE_URL + 'options';  // URL to web api
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  getOptions(): Promise<SubscriberOption[]> {
    return this.http.get(this.serviceUrl)
      .toPromise()
      .then(response => response.json() as SubscriberOption[])
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.serviceUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string): Promise<SubscriberOption> {
    return this.http
      .post(this.serviceUrl, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }
  update(option: SubscriberOption): Promise<SubscriberOption> {
    const url = `${this.serviceUrl}/${option.id}`;
    return this.http
      .put(url, JSON.stringify(option), {headers: this.headers})
      .toPromise()
      .then(() => option)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
