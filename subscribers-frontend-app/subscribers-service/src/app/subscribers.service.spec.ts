/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SubscribersService } from './subscribers.service';

describe('SubscribersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubscribersService]
    });
  });

  it('should ...', inject([SubscribersService], (service: SubscribersService) => {
    expect(service).toBeTruthy();
  }));
});
