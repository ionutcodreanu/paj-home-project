/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SubscriberOptionsService } from './subscriber-options.service';

describe('SubscriberOptionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubscriberOptionsService]
    });
  });

  it('should ...', inject([SubscriberOptionsService], (service: SubscriberOptionsService) => {
    expect(service).toBeTruthy();
  }));
});
