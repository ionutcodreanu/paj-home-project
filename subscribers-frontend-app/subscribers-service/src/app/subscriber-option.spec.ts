import {SubscriberOption} from './subscriber-option';

describe('SubscriberOption', () => {
  it('should create an instance', () => {
    expect(new SubscriberOption()).toBeTruthy();
  });
  it('should accept values in the constructor', () => {
    let subscriberOption = new SubscriberOption({
      name: 'Option 1',
      id: 15
    });
    expect(subscriberOption.name).toEqual('Option 1');
    expect(subscriberOption.id).toEqual(15);
  });
});
