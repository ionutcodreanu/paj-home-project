import {Injectable} from '@angular/core';
import {Http, Headers} from "@angular/http";
import {environment} from '../environments/environment';
import {Subscribers} from "./subscribers";
import {SubscriberOption} from "./subscriber-option";

@Injectable()
export class SubscribersService {
  private serviceUrl = environment.API_BASE_URL + 'subscribers';  // URL to web api
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  get(): Promise<Subscribers[]> {
    return this.http.get(this.serviceUrl)
      .toPromise()
      .then(response => {
        let data = response.json();
        let returnData = [];
        data.forEach(model => returnData.push(new Subscribers(model)));
        return returnData;
      })
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.serviceUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string, email: string, options: SubscriberOption[]): Promise<Subscribers> {
    let data = {
      name: name,
      email: email,
      subscribingOptions: options
    };
    return this.http
      .post(this.serviceUrl, JSON.stringify(data), {headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  update(option: Subscribers): Promise<Subscribers> {
    const url = `${this.serviceUrl}/${option.id}`;
    return this.http
      .put(url, JSON.stringify(option), {headers: this.headers})
      .toPromise()
      .then(() => option)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
