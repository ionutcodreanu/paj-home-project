import {Component, OnInit} from '@angular/core';
import {SubscriberOption} from "../subscriber-option";
import {SubscriberOptionsService} from "../subscriber-options.service";
import {Response} from "@angular/http";

@Component({
  selector: 'app-subscriber-options-list',
  templateUrl: './subscriber-options-list.component.html',
  styleUrls: ['./subscriber-options-list.component.css']
})
export class SubscriberOptionsListComponent implements OnInit {

  subscriberOptions: SubscriberOption[];
  subscriberOptionModel = new SubscriberOption();
  errorMessage: string;

  constructor(private subscriberOptionsService: SubscriberOptionsService) {
  }

  getOptions(): void {
    this.subscriberOptionsService
      .getOptions()
      .then(
        options => {
          this.subscriberOptions = options;
        },
        error => {
          this.errorMessage = <any>error;
        }
      );
  }

  add(): void {
    let name = this.subscriberOptionModel.name.trim();
    if (!name) {
      return;
    }
    this.subscriberOptionsService.create(name)
      .then(option => {
        this.subscriberOptions.push(option);
        this.subscriberOptionModel = new SubscriberOption();
      });
  }

  save(): void {
    if (this.subscriberOptionModel.id == null) {
      this.add();
    } else {
      this.edit();
    }

  }

  delete(option: SubscriberOption): void {
    this.subscriberOptionsService
      .delete(option.id)
      .then(() => {
        this.subscriberOptions = this.subscriberOptions.filter(h => h !== option);
      });
  }

  toggleEdit(option: SubscriberOption): void {
    this.subscriberOptionModel = option;
  }

  ngOnInit(): void {
    this.getOptions();
  }


  private edit(): void {
    this.subscriberOptionsService.update(this.subscriberOptionModel).then(option => {
      this.subscriberOptionModel = new SubscriberOption();
    });
  }
}
