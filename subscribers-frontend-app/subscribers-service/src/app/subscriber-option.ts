export class SubscriberOption {
  id: number;
  name: string = ''
  checked: boolean = false;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
