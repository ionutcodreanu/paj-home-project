import {SubscriberOption} from "./subscriber-option";
export class Subscribers {
  id: number;
  name: string;
  email: string;
  subscribingOptions: SubscriberOption[];

  constructor(values: Object = {}) {
    if (values.hasOwnProperty('subscribingOptions')) {
      this.subscribingOptions = new Array<SubscriberOption>();
      for (let option of values['subscribingOptions']) {
        this.subscribingOptions.push(new SubscriberOption(option));
      }
      delete values['subscribingOptions'];
    }
    Object.assign(this, values);
  }

  public getOptionsAsString(): string {
    return this.subscribingOptions.map(option => option.name).join(', ');
  }
}
