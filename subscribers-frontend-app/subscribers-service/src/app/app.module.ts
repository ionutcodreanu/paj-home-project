import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {SubscriberOptionsListComponent} from './subscriber-options-list/subscriber-options-list.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {SubscribeComponent} from './subscribe/subscribe.component';
import {SubscribersComponent} from './subscribers/subscribers.component';
import {SubscriberOptionsService} from './subscriber-options.service';
import {SubscribersService} from "./subscribers.service";

const appRoutes: Routes = [
  {
    path: 'admin/options',
    component: SubscriberOptionsListComponent,
    data: {title: 'SubscriberOptionsList'}
  },
  {
    path: 'subscribe',
    component: SubscribeComponent,
  },
  {
    path: 'subscribers',
    component: SubscribersComponent,
  },
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SubscriberOptionsListComponent,
    PageNotFoundComponent,
    SubscribeComponent,
    SubscribersComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [SubscriberOptionsService, SubscribersService],
  bootstrap: [AppComponent],
})
export class AppModule {
}
