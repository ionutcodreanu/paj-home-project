import {Component, OnInit} from '@angular/core';
import {Subscribers} from "../subscribers";
import {SubscribersService} from "../subscribers.service";
import {Router} from "@angular/router";
import {SubscriberOptionsService} from "../subscriber-options.service";
import {SubscriberOption} from "../subscriber-option";

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  subscriberModel = new Subscribers();
  subscriberOptions: SubscriberOption[];
  private errorMessage: string;

  constructor(private subscribersService: SubscribersService,
              private router: Router,
              private subscribersOptionsService: SubscriberOptionsService) {
  }

  ngOnInit() {
    this.getOptions();
  }

  add(): void {
    let name = this.subscriberModel.name.trim();
    let email = this.subscriberModel.email.trim();
    if (!name || !email) {
      return;
    }

    let options = this.getFromOptions();

    this.subscribersService
      .create(name, email, options)
      .then(() => this.router.navigate(['/subscribers']));

  }

  private getOptions(): void {
    this.subscribersOptionsService
      .getOptions()
      .then(
        options => {
          this.subscriberOptions = options;
        },
        error => {
          this.errorMessage = <any>error;
        }
      );
  }

  private getFromOptions(): SubscriberOption[] {
    return this.subscriberOptions
      .filter(options => options.checked)
      .map(option => option);
  }
}
