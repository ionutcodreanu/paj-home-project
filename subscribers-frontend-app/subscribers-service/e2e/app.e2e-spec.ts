import { SubscribersServicePage } from './app.po';

describe('subscribers-service App', function() {
  let page: SubscribersServicePage;

  beforeEach(() => {
    page = new SubscribersServicePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
