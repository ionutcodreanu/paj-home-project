-- Dumping database structure for subscribers_service
DROP DATABASE IF EXISTS `subscribers_service`;
CREATE DATABASE IF NOT EXISTS `subscribers_service` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `subscribers_service`;

-- Dumping structure for table subscribers_service.subscriber
DROP TABLE IF EXISTS `Subscriber`;
CREATE TABLE IF NOT EXISTS `Subscriber` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- Dumping structure for table subscribers_service.subscriber_option
DROP TABLE IF EXISTS `Subscriber_Option`;
CREATE TABLE IF NOT EXISTS `Subscriber_Option` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subscriberId` int(10) unsigned NOT NULL,
  `optionId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriberId` (`subscriberId`,`optionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- Dumping structure for table subscribers_service.subscribingoption
DROP TABLE IF EXISTS `SubscribingOption`;
CREATE TABLE IF NOT EXISTS `SubscribingOption` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

