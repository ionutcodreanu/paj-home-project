package com.restaurantplus.subscribers.resource;

import com.restaurantplus.subscribers.model.SubscribingOption;
import com.restaurantplus.subscribers.repository.OptionRepository;
import com.restaurantplus.subscribers.repository.OptionRepositoryJpa;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
@Path("options")
public class OptionResource {

  private OptionRepository optionsRepository = new OptionRepositoryJpa();

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<SubscribingOption> findAllOptions() {
    return this.optionsRepository.findAll();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{optionId}")
  public SubscribingOption findSubscriber(@PathParam("optionId") Long optionId) {
    return this.optionsRepository.findById(optionId);
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public SubscribingOption createSubscriber(SubscribingOption subscribingOption) {
    optionsRepository.create(subscribingOption);
    return subscribingOption;
  }

  @PUT
  @Path("{optionId}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public SubscribingOption updateSubscriber(SubscribingOption subscribingOption, @PathParam("optionId") Long optionId) {
    subscribingOption.setId(optionId);
    return optionsRepository.update(subscribingOption);
  }

  @DELETE
  @Path("{optionId}")
  @Produces(MediaType.APPLICATION_JSON)
  public void deleteSubscriber(@PathParam("optionId") Long optionId) {
    optionsRepository.delete(optionId);
  }
}
