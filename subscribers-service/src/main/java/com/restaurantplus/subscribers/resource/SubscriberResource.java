package com.restaurantplus.subscribers.resource;

import com.restaurantplus.subscribers.model.Subscriber;
import com.restaurantplus.subscribers.repository.SubscriberRepository;
import com.restaurantplus.subscribers.repository.SubscriberRepositoryJpa;
import com.restaurantplus.subscribers.repository.SubscriberRepositoryStub;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
@Path("subscribers")
public class SubscriberResource {

    private SubscriberRepository subscriberRepository = new SubscriberRepositoryJpa();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Subscriber> findAllSubscribers() {
        return this.subscriberRepository.findAllSubscribers();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{subscriberId}")
    public Subscriber findSubscriber(@PathParam("subscriberId") Long subscriberId) {
        return this.subscriberRepository.findSubscriber(subscriberId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Subscriber createSubscriber(Subscriber subscriber) {
        subscriberRepository.create(subscriber);
        return subscriber;
    }

    @PUT
    @Path("{subscriberId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Subscriber updateSubscriber(Subscriber subscriber, @PathParam("subscriberId") Long subscriberId) {
        subscriber.setId(subscriberId);
        return subscriberRepository.update(subscriber);
    }
    @DELETE
    @Path("{subscriberId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteSubscriber(@PathParam("subscriberId") Long subscriberId) {
        subscriberRepository.delete(subscriberId);
    }
}
