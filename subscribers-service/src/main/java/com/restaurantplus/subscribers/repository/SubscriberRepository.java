package com.restaurantplus.subscribers.repository;

import com.restaurantplus.subscribers.model.Subscriber;

import java.util.List;

/** @author Ionut Codreanu <ionutcodreanu@outlook.com> */
public interface SubscriberRepository {
  List<Subscriber> findAllSubscribers();

  Subscriber findSubscriber(Long subscriberId);

  Subscriber create(Subscriber subscriber);

  Subscriber update(Subscriber subscriber);

  void delete(Long subscriberId);
}
