package com.restaurantplus.subscribers.repository;

import com.restaurantplus.subscribers.model.SubscribingOption;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.commons.beanutils.BeanUtils;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
public class OptionRepositoryJpa implements OptionRepository {

  private final EntityManager em;

  public OptionRepositoryJpa() {
    //use dependency injection
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Subscribers");
    em = emf.createEntityManager();
  }

  @Override
  public List<SubscribingOption> findAll() {
    try {
      CriteriaBuilder cb = em.getCriteriaBuilder();
      CriteriaQuery<SubscribingOption> cq = cb.createQuery(SubscribingOption.class);
      Root<SubscribingOption> rootEntry = cq.from(SubscribingOption.class);
      CriteriaQuery<SubscribingOption> all = cq.select(rootEntry);
      TypedQuery<SubscribingOption> allQuery = em.createQuery(all);
      return allQuery.getResultList();
    } catch (Exception exc) {
      System.out.println(exc.getMessage());
    }

    return new LinkedList<SubscribingOption>();
  }

  @Override
  public SubscribingOption findById(Long optionId) {
    return em.find(SubscribingOption.class, optionId);
  }

  @Override
  public SubscribingOption create(SubscribingOption subscribingOption) {
    em.getTransaction().begin();
    em.persist(subscribingOption);
    em.getTransaction().commit();

    return subscribingOption;
  }

  @Override
  public SubscribingOption update(SubscribingOption subscribingOptionToSave) {
    SubscribingOption attached = em.find(SubscribingOption.class, subscribingOptionToSave.getId());
    em.getTransaction().begin();
    if (attached == null) {
      attached = new SubscribingOption();
      em.persist(attached);
    }
    try {
      BeanUtils.copyProperties(attached, subscribingOptionToSave);
    } catch (IllegalAccessException exc) {
      System.out.println("Log here");
    } catch (InvocationTargetException exc) {
      System.out.println("Log here2");
    }
    em.getTransaction().commit();

    return attached;

  }

  @Override
  public void delete(Long optionId) {
    SubscribingOption attached = em.find(SubscribingOption.class, optionId);
    em.getTransaction().begin();
    em.remove(attached);
    em.getTransaction().commit();

  }
}
