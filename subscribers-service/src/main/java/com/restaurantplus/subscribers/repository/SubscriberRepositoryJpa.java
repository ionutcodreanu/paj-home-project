package com.restaurantplus.subscribers.repository;

import com.restaurantplus.subscribers.model.Subscriber;
import org.apache.commons.beanutils.BeanUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
public class SubscriberRepositoryJpa implements SubscriberRepository {

  private static EntityManager em;

  public SubscriberRepositoryJpa() {
    //use dependency injection
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Subscribers");
    em = emf.createEntityManager();
  }

  @Override
  public List<Subscriber> findAllSubscribers() {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Subscriber> cq = cb.createQuery(Subscriber.class);
    Root<Subscriber> rootEntry = cq.from(Subscriber.class);
    CriteriaQuery<Subscriber> all = cq.select(rootEntry);
    TypedQuery<Subscriber> allQuery = em.createQuery(all);
    return allQuery.getResultList();
  }

  @Override
  public Subscriber findSubscriber(Long subscriberId) {
    return em.find(Subscriber.class, subscriberId);
  }

  @Override
  public Subscriber create(Subscriber subscriber) {
    em.getTransaction().begin();
    em.persist(subscriber);
    em.getTransaction().commit();

    return subscriber;
  }

  @Override
  public Subscriber update(Subscriber subscriberToSave) {
    Subscriber attached = em.find(Subscriber.class, subscriberToSave.getId());
    em.getTransaction().begin();
    if (attached == null) {
      attached = new Subscriber();
      em.persist(attached);
    }
    try {
      BeanUtils.copyProperties(attached, subscriberToSave);
    } catch (IllegalAccessException exc) {
      System.out.println("Log here");
    } catch (InvocationTargetException exc) {
      System.out.println("Log here2");
    }
    em.getTransaction().commit();

    return attached;
  }

  @Override
  public void delete(Long subscriberId) {
    Subscriber attached = em.find(Subscriber.class, subscriberId);
    em.getTransaction().begin();
    em.remove(attached);
    em.getTransaction().commit();
  }
}
