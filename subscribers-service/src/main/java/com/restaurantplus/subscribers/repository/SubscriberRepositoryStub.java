package com.restaurantplus.subscribers.repository;

import com.restaurantplus.subscribers.model.Subscriber;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
public class SubscriberRepositoryStub implements SubscriberRepository {

  @Override
  public List<Subscriber> findAllSubscribers() {
    List<Subscriber> subscriberList = new ArrayList<Subscriber>();

    Subscriber subscriber1 = new Subscriber();
    subscriber1.setEmail("email1@localhost.com");
    subscriber1.setName("Ionut Codreanu");
    subscriber1.setId(1L);

    subscriberList.add(subscriber1);

    Subscriber subscriber2 = new Subscriber();
    subscriber2.setEmail("email2@localhost.com");
    subscriber2.setName("Ionut Codreanu Clone");
    subscriber2.setId(1L);
    subscriberList.add(subscriber2);

    return subscriberList;
  }

  @Override
  public Subscriber findSubscriber(Long subscriberId) {
    Subscriber subscriber1 = new Subscriber();

    subscriber1.setEmail("email1@localhost.com");
    subscriber1.setName("Ionut Codreanu");
    subscriber1.setId(subscriberId);

    return subscriber1;
  }

  @Override
  public Subscriber create(Subscriber subscriber) {
    return subscriber;
  }

  @Override
  public Subscriber update(Subscriber subscriber) {
    return null;
  }

  @Override
  public void delete(Long subscriberId) {
  }
}
