package com.restaurantplus.subscribers.repository;

import com.restaurantplus.subscribers.model.SubscribingOption;
import java.util.List;

/**
 * @author Ionut Codreanu <ionutcodreanu@outlook.com>
 */
public interface OptionRepository {

  List<SubscribingOption> findAll();

  SubscribingOption findById(Long subscriberId);

  SubscribingOption create(SubscribingOption subscriber);

  SubscribingOption update(SubscribingOption subscriber);

  void delete(Long subscriberId);
}
