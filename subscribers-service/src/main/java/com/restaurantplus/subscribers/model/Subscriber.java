package com.restaurantplus.subscribers.model;

import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/** @author Ionut Codreanu <ionutcodreanu@outlook.com> */
@XmlRootElement
@Entity
public class Subscriber {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  private Long id;

  private String email;
  private String name;

  @ManyToMany
  @JoinTable(
      name = "Subscriber_Option",
      joinColumns = @JoinColumn(name = "subscriberId", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "optionId", referencedColumnName = "id")
  )
  private List<SubscribingOption> subscribingOptions;


  public Subscriber(){}

  public Subscriber(Subscriber subscriber) {
    this.setName(subscriber.getName());
    this.setEmail(subscriber.getEmail());
    this.setId(subscriber.getId());
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<SubscribingOption> getSubscribingOptions() {
    return subscribingOptions;
  }

  public void setSubscribingOptions(List<SubscribingOption> subscribingOptions) {
    this.subscribingOptions = subscribingOptions;
  }
}
