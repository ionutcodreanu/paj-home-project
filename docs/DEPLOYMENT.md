### How to build java projects

Go to subscribers-service or ratings-service and run following command:

`mvn clean package docker:build`

This command will build de Java project, will create the .war artifact and will build
the docker image.

To create the image for the database server, you should run one of the following 
commands depending on the service:

`docker build -f DockerfileDatabase -t restaurantplus/ratings-service-db .`

`docker build -f DockerfileDatabase -t restaurantplus/subscribers-service-db .`

To run projects, you should have built all 4 images:
* restaurantplus/ratings-service-db
* restaurantplus/ratings-service
* restaurantplus/subscribers-service-db 
* restaurantplus/subscribers-service


### Subscribers Service FrontEnd app deployment
Run following commands to build:
  
`ng build --prod`

`docker build -t restaurantplus/subscribers-service-frontend .`

You can check your images by running following command:

`docker images`

After all the images are in place, go to project root and run following command:

`docker-compose up`