Project documentation
=====================

Customer loyalty platform for self-service restaurants
------------------------------------------------------

### Project description

The project aims to provide to self-service restaurants an integrated solution to improve their
customer loyalty by increasing the retention and satisfaction levels fructifying widening use of 
smart devices. In order to achieve these objectives, the platform will have the following components:
* How receiving and analyzing feedback
* How to subscribe and content management
* How marketing

In this project we will implement prototype of the first two modules, namely the application for 
receiving and analyzing feedback and app subscription and content management.

#### Module for receiving and analyzing feedback from customers

For restaurant customers we will provide an interface to a web portal where they can evaluate their 
experience with the restaurant services and they will be able to make comments about their 
experience and satisfaction about restaurant services.

Restaurant staff will be able to view statistics about feedback obtained on specific periods 
of time (days, weeks, months) and also will have an interface where will be able to view comments
received from customers.

#### Module for customer subscription and content management

Restaurant customers will have the opportunity to subscribe to the restaurant newsletter where they 
can receive information on the daily menu or can receive various promotions from the restaurant.

Restaurant customers will subscribe to the restaurant newsletter through an online form where they
can enter their email address and can choose what type of newsletter they would like to receive.

Restaurant administrators can visualize the list with subscribers and their options. Also they will 
manage the content for the newsletter.

### Technical description

For both modules of the platform will be used a client-server architecture. The project will be
versioned using Git software and will be hosted in a GitLab private repo. The applications both 
the frontends and the backends will be deployed with [Docker](https://www.docker.com/).
 
The server side applications will be developed using Java language with Java Enterprise Edition
Framework. The build automation will be managed with Maven. As application webserver will be used
WildFly 10.

For data persistence will be used a MySql server, version 5.7. For every application will be deployed 
an individual server. For the application persistence layer will be used Java Persistence API(JPA) 
using Hibernate provider.

The backend applications will expose an RESTfull API. For the implementation will be used the Jersey 
framework which respect the JAX-RX standard.

The frontend applications will be implemented using a Javascript framework, the will communicate with 
the backend applications via REST protocol. 

No security concerns will be addressed in this stage like authentication and authorization.

#### Module for receiving and analyzing feedback from customers

In the following image is displayed the database schema for this module. The only table used in 
this module is **ratings** table that store information about customer ratings.

![Database structure][ratings-service-database]

The next image ilustre a diagram with JPA entities from these module

![Entities Diagram][ratings-service-entities]

#### Module for customer subscription and content management

The following two images ilustrate the database structure and the JPA entities diagram.

![Subscription Database Diagram][subscriptions-service-database]
![Subscription Entities Diagram][subscriptions-service-entitites]


[ratings-service-entities]: images/ratings-service-entities.png
[ratings-service-database]: images/ratings-service-database.png
[subscriptions-service-entitites]: images/subscribers-service-entities.png
[subscriptions-service-database]: images/subscribers-service-database.png